import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Project } from '../classes/project';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private projectUrl: string;
  private homeUrl: string;

  constructor(private http: HttpClient ) {
    this.projectUrl = 'api/project';
    this.homeUrl = 'api/home'
  }

  public save(project: Project): Observable<Project>  {
    console.log('service', project, this.projectUrl)
    return this.http.post<Project>(this.projectUrl, project);
    // this.router.navigate(['login'], { queryParams: { returnUrl: this.parkingUrl }});
  }

  public getAllProjects(): Observable<Project[]> {

    return this.http.get<Project[]>(this.homeUrl);

  }

  public getProjectById(id: number): Observable<Project> {
    return this.http.get<Project>(this.projectUrl + '/' +id);
  }

  public update(project: Project): Observable<Project> {
    console.log(project)
    return this.http.put<Project>(this.projectUrl + '/' +project.id, project);
  }

  public delete(id: number): Observable<Project> {
    return this.http.delete<Project>(this.projectUrl + '/' +id);
  }
}
