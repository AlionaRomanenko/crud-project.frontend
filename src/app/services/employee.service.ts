import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from '../classes/employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private employeeUrl: string;
  private serviceUrl : string;
  private projectUrl : string;

  constructor(private http: HttpClient ) {
    this.employeeUrl = 'api/employee';
    this.serviceUrl = 'api/employees';
    this.projectUrl = 'api/project'

  }
  public save(employee: Employee): Observable<Employee>  {
    return this.http.post<Employee>(this.employeeUrl, employee);
  }

  public getAllEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.serviceUrl);

  }

  public getEmployeeById(id: number): Observable<Employee> {
    return this.http.get<Employee>(this.employeeUrl + '/' +id);
  }

  public update(employee: Employee): Observable<Employee> {
    console.log(employee)
    return this.http.put<Employee>(this.employeeUrl + '/' +employee.id, employee);
  }

  public delete(id: number): Observable<Employee> {
    return this.http.delete<Employee>(this.employeeUrl + '/' +id);
  }

  public getAllEmployeesByProject(id: number): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.projectUrl + '/' +id+'/employees');

  }

  public saveWithProjectId(employee: Employee): Observable<Employee>  {
    return this.http.post<Employee>(this.projectUrl + '/' +employee.id +'/employee', employee);
  }

  public deleteUser(id: number): Observable<Employee> {
    return this.http.delete<Employee>(this.employeeUrl + id);
  }
}
