import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddProjectComponent } from './add-project/add-project.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { HomeComponent } from './home/home.component';
import { EmloyeeListComponent } from './emloyee-list/emloyee-list.component';
import { EmployeeComponent } from './employee/employee.component';
import { ProjectComponent } from './project/project.component';
import { EmployeeProjectComponentComponent } from './employee-project-component/employee-project-component.component';
import { AddEmployeeProjectComponent } from './add-employee-project/add-employee-project.component';


const routes: Routes = [
  { path: 'project', component: AddProjectComponent },
  { path: 'employee', component: AddEmployeeComponent },
  { path: 'home', component: HomeComponent},
  { path: 'employees', component: EmloyeeListComponent},
  { path: 'employee/:id', component: EmployeeComponent},
  { path: 'project/:id', component: ProjectComponent},
  { path: 'project/:id/employees', component:EmployeeProjectComponentComponent},
  { path: 'project/:id/employee', component: AddEmployeeProjectComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
