import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddProjectComponent } from './add-project/add-project.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { HomeComponent } from './home/home.component';
import { DialogComponent } from './dialog/dialog.component';
import { EmloyeeListComponent } from './emloyee-list/emloyee-list.component';
import { EmployeeComponent } from './employee/employee.component';
import { ProjectComponent } from './project/project.component';
import { EmployeeProjectComponentComponent } from './employee-project-component/employee-project-component.component';
import { AddEmployeeProjectComponent } from './add-employee-project/add-employee-project.component';

// import { MatMomentDateModule } from "@angular/material-moment-adapter";

@NgModule({
  declarations: [
    AppComponent,
    AddProjectComponent,
    AddEmployeeComponent,
    HomeComponent,
    DialogComponent,
    EmloyeeListComponent,
    EmployeeComponent,
    ProjectComponent,
    EmployeeProjectComponentComponent,
    AddEmployeeProjectComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatInputModule,
        MatCardModule,
        MatFormFieldModule,
        MatButtonModule,
        MatListModule,
        MatDividerModule,
        FormsModule,
        ReactiveFormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        HttpClientModule,
        MatSidenavModule,
        // HttpClient,
        // MatMomentDateModule
    ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule { }
