import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeProjectComponentComponent } from './employee-project-component.component';

describe('EmployeeProjectComponentComponent', () => {
  let component: EmployeeProjectComponentComponent;
  let fixture: ComponentFixture<EmployeeProjectComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeProjectComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeProjectComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
