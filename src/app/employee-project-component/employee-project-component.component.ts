import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../classes/employee';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee-project-component',
  templateUrl: './employee-project-component.component.html',
  styleUrls: ['./employee-project-component.component.scss']
})
export class EmployeeProjectComponentComponent implements OnInit {

  userId: number;
  employees: Employee[]
  constructor(private route: ActivatedRoute,private employeeService: EmployeeService) { }

  ngOnInit() {
    this.userId = this.route.snapshot.params.id;
    this.employeeService.getAllEmployeesByProject(this.userId).subscribe(employees => this.employees = employees);
    console.log(this.employees);
  }

}
