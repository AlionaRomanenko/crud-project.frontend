import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../services/project.service';
import { Project } from '../classes/project';
import * as _moment from 'moment';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatSnackBarModule } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class ProjectComponent implements OnInit {

  userId: number;
  formGroup: FormGroup;
  nameregex: RegExp = /^[a-zA-Z 0-9-]+$/;
  dateregex: RegExp = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;//create proper validation
  project: Project = new Project();
  updateClicked = false;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
              private router: Router, private projectService: ProjectService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.userId = this.route.snapshot.params.id;
    if(!this.userId) {
      alert("Invalid action.")
      this.router.navigate(['home']);
      return;
    }
    console.log(this.userId);
    this.formGroup = this.formBuilder.group({
      id: [''],
      name: [null, [Validators.required, Validators.pattern(this.nameregex)]],
      startDate:[null, [Validators.required]],
      endDate: [null, [Validators.required]]

    });
    this.projectService.getProjectById(+this.userId)
      .subscribe( data => {
        console.log(data)
        this.formGroup.setValue(data);
      });
  }

  getErrorName() {
    return this.formGroup.get('name').hasError('required') ? 'Field is required' :
      this.formGroup.get('name').hasError('pattern') ? 'Invalid name of' : '';
  }

  getErrorDate() {
    return this.formGroup.get('startPicker').hasError('required') ? 'Field is required':
      this.formGroup.get('endPicker').hasError('required') ? 'Field is required': '';
  }

  openDialog( message: string): MatDialogRef<DialogComponent> {
    console.log(message);
    return this.dialog.open(DialogComponent, {
      width: '350px',
      data: {message}
    });
  }


  public onUpdateClick(): void {
    this.updateClicked = true;
  }

  public onDeleteClick(): void {
    this.updateClicked = false;
  }
  public onClick(): void {
    this.router.navigate(['/project/'+this.userId + '/employees']);
  }
  public onClick1(): void {
    this.router.navigate(['/project/'+this.userId + '/employee']);
  }

  onSubmit(formGroup) {
    console.log(formGroup);
    this.project.id = formGroup.id;
    this.project.name = formGroup.name;
    this.project.startDate = formGroup.startDate;
    this.project.endDate = formGroup.endDate;
    console.log(this.project);
    if(this.updateClicked) {
      this.projectService.update(this.project)
      .subscribe( data => {
        this.openDialog(("Project updated successfully."));
     },
     err => {
      this.openDialog(err.error);

     });
     this.router.navigate(['home']);
     }else {
      this.projectService.delete(this.project.id)
      .subscribe( data => {
        this.openDialog(("Project deleted successfully."));
     },
     err => {
      this.openDialog(err.error);

     });
    }
}
}



