import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectService } from '../services/project.service';
import { Project } from '../classes/project';
import * as _moment from 'moment';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDialogRef, MatDialog, MatSnackBar, MatSnackBarModule } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AddProjectComponent implements OnInit {
  formGroup: FormGroup;
  nameregex: RegExp = /^[a-zA-Z 0-9-]+$/;
  dateregex: RegExp = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;//create proper validation
  project: Project = new Project();


  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
              private router: Router, private projectService: ProjectService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      name: [null, [Validators.required, Validators.pattern(this.nameregex)]],
      startPicker:[null, [Validators.required]],
      endPicker: [null, [Validators.required]]
    });
  }

  getErrorName() {
    return this.formGroup.get('name').hasError('required') ? 'Field is required' :
      this.formGroup.get('name').hasError('pattern') ? 'Invalid name of' : '';
  }

  getErrorDate() {
    return this.formGroup.get('startPicker').hasError('required') ? 'Field is required':
      this.formGroup.get('endPicker').hasError('required') ? 'Field is required': '';
  }

  openDialog( message: string): MatDialogRef<DialogComponent> {
    console.log(message);
    return this.dialog.open(DialogComponent, {
      width: '350px',
      data: {message}
    });
  }

  onSubmit(formGroup) {
   console.log(this.project);
   this.project.name = formGroup.name;
   this.project.startDate = formGroup.startPicker.format('YYYY-MM-DD');
   this.project.endDate = formGroup.endPicker.format('YYYY-MM-DD');
   console.log(this.project);
   this.projectService.save(this.project)
   .subscribe( data => {
    this.openDialog(("Project created successfully."));
 },
 err => {
   this.openDialog(err.error);

 });
}
}

