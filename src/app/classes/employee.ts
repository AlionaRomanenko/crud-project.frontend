export class Employee {
    id: number;
    firstName: string;
    lastName: string;
    salary: number;
    phoneNumber: string;
    birthDate: string;
    projectName: string;
}
