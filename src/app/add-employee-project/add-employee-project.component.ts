import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Employee } from '../classes/employee';
import * as _moment from 'moment';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { EmployeeService } from '../services/employee.service';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-add-employee-project',
  templateUrl: './add-employee-project.component.html',
  styleUrls: ['./add-employee-project.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class AddEmployeeProjectComponent implements OnInit {
  userId: number;
  formGroup: FormGroup;
  nameregex: RegExp = /^[A-Z][a-zA-Z-]*$/;
  phoneregex: RegExp = /^380\d{9}$/
  dateregex: RegExp = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;//create proper validation
  onlyPositiveIntegersregex: RegExp = /^[1-9]+[0-9]*$/;
  employee: Employee = new Employee();

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute,
              private router: Router, private employeeService: EmployeeService,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.userId = this.route.snapshot.params.id;
    this.createForm();
  }

  getErrorFirstName() {
    return this.formGroup.get('firstName').hasError('required') ? 'Field is required' :
      this.formGroup.get('firstName').hasError('pattern') ? 'Invalid first name of employee' : '';
  }

  getErrorLastName() {
    return this.formGroup.get('lastName').hasError('required') ? 'Field is required' :
      this.formGroup.get('lastName').hasError('pattern') ? 'Invalid last name of employee' : '';
  }

  getErrorPhoneNumber() {
    return this.formGroup.get('phoneNumber').hasError('required') ? 'Field is required' :
      this.formGroup.get('phoneNumber').hasError('pattern') ? 'Phone number should start with 380 and contain 12 numbers' : '';
  }

  getErrorSalary() {
    return this.formGroup.get('salary').hasError('required') ? 'Field is required' :
      this.formGroup.get('salary').hasError('pattern') ? 'Salary should be positive integer' : '';
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      firstName: [null, [Validators.required, Validators.pattern(this.nameregex)]],
      lastName: [null, [Validators.required, Validators.pattern(this.nameregex)]],
      phoneNumber: [null,[Validators.required, Validators.pattern(this.phoneregex)]],
      salary: [null, [Validators.required, Validators.pattern(this.onlyPositiveIntegersregex)]],
      birthPicker: [null, [Validators.required]]
    });
  }

  openDialog( message: string): MatDialogRef<DialogComponent> {
    console.log(message);
    return this.dialog.open(DialogComponent, {
      width: '350px',
      data: {message}
    });
  }

  public onClick(): void {
    this.router.navigate(['/project/'+this.userId + '/employees']);
  }
  onSubmit(formGroup) {
    console.log(this.employee);
    this.employee.id = this.userId;
    this.employee.firstName = formGroup.firstName;
    this.employee.lastName = formGroup.lastName;
    this.employee.phoneNumber = formGroup.phoneNumber;
    this.employee.salary = formGroup.salary;
    this.employee.birthDate = formGroup.birthPicker.format('YYYY-MM-DD');
    console.log(this.employee);
    this.employeeService.saveWithProjectId(this.employee)
      .subscribe( data => {
        this.openDialog(("Employee created successfully."));
     },
     err => {
      this.openDialog(err.error);
 
     });
    }
}
