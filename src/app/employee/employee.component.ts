import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Employee } from '../classes/employee';
import { EmployeeService } from '../services/employee.service';
import { DialogComponent } from '../dialog/dialog.component';
import * as _moment from 'moment';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatDialogRef, MatDialog, MatSnackBar } from '@angular/material';
export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM-DD',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class EmployeeComponent implements OnInit {
  nameregex: RegExp = /^[A-Z][a-zA-Z-]*$/;
  phoneregex: RegExp = /^380\d{9}$/
  dateregex: RegExp = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;//create proper validation
  onlyPositiveIntegersregex: RegExp = /^[1-9]+[0-9]*$/;
  employee: Employee = new Employee();
  formGroup: FormGroup;
  updateClicked = false;
  constructor(private formBuilder: FormBuilder,private router: Router,
              public dialog: MatDialog,private employeeService: EmployeeService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    let userId = this.route.snapshot.params.id;
    if(!userId) {
      alert("Invalid action.")
      this.router.navigate(['home']);
      return;
    }
    console.log(userId);
    this.formGroup = this.formBuilder.group({
      id: [''],
      firstName: [null, [Validators.required, Validators.pattern(this.nameregex)]],
      lastName: [null, [Validators.required, Validators.pattern(this.nameregex)]],
      phoneNumber: [null,[Validators.required, Validators.pattern(this.phoneregex)]],
      salary: [null, [Validators.required, Validators.pattern(this.onlyPositiveIntegersregex)]],
      birthDate: [null, [Validators.required]]
    });
    this.employeeService.getEmployeeById(+userId)
      .subscribe( data => {
        console.log(data)
        this.formGroup.setValue(data);
      });
  }

  getErrorFirstName() {
    return this.formGroup.get('firstName').hasError('required') ? 'Field is required' :
      this.formGroup.get('firstName').hasError('pattern') ? 'Invalid first name of employee' : '';
  }

  getErrorLastName() {
    return this.formGroup.get('lastName').hasError('required') ? 'Field is required' :
      this.formGroup.get('lastName').hasError('pattern') ? 'Invalid last name of employee' : '';
  }

  getErrorPhoneNumber() {
    return this.formGroup.get('phoneNumber').hasError('required') ? 'Field is required' :
      this.formGroup.get('phoneNumber').hasError('pattern') ? 'Phone number should start with 380 and contain 12 numbers' : '';
  }

  getErrorSalary() {
    return this.formGroup.get('salary').hasError('required') ? 'Field is required' :
      this.formGroup.get('salary').hasError('pattern') ? 'Salary should be positive integer' : '';
  }

  openDialog( message: string): MatDialogRef<DialogComponent> {
    console.log(message);
    return this.dialog.open(DialogComponent, {
      width: '350px',
      data: {message}
    });
  }

  public onUpdateClick(): void {
    this.updateClicked = true;
  }

  public onDeleteClick(): void {
    this.updateClicked = false;
  }

  onSubmit(formGroup) {
    console.log(formGroup);
    this.employee.id = formGroup.id;
    this.employee.firstName = formGroup.firstName;
    this.employee.lastName = formGroup.lastName;
    this.employee.phoneNumber = formGroup.phoneNumber;
    this.employee.salary = formGroup.salary;
    this.employee.birthDate = formGroup.birthDate;
    console.log(this.employee);
    if(this.updateClicked) {
      this.employeeService.update(this.employee)
      .subscribe( data => {
        this.openDialog(("Employee updated successfully."));
     },
     err => {
      this.openDialog(err.error);
 
     });
     this.router.navigate(['home']);
     }else {
      this.employeeService.delete(this.employee.id)
      .subscribe( data => {
        this.openDialog(("Employee deleted successfully."));
     },
     err => {
      this.openDialog(err.error);
 
     });
     this.router.navigate(['home']);
    }
}
}
