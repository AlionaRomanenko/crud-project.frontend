import { Component, OnInit } from '@angular/core';
import { Employee } from '../classes/employee';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-emloyee-list',
  templateUrl: './emloyee-list.component.html',
  styleUrls: ['./emloyee-list.component.scss']
})
export class EmloyeeListComponent implements OnInit {

  employees: Employee[];

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
    this.employeeService.getAllEmployees().subscribe(employees => this.employees = employees);
    console.log(this.employees);
  }
}
